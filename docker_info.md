
_**Here are instructions to download your files from the containers:**_

1. Go to your container, and choose 'New Terminal'
2. In the terminal window, type: 
`tar cvzf 2017-HTS-materials.tar.gz 2017-HTS-materials`. This will create a new file (archived and compressed) called 2017-HTS-materials.tar.gz. 
3. Go back to the file browser and click on the new file. You should be prompted to download.
4. Once you have the file, your system's archive program should be able to open it and allow you to extract to a directory on your computer.

Windows users: There is an additional layer of complexity if you try any of the below from windows. I am still working out the kinks in using the 'docker for windows' program. Mark McCahill advised me to install virtual box on windows and run docker from within an ubuntu VM. Probably the best way to go, if you understand what all that means. If not, stay tuned and I'll send a set of windows-specific instructions soon. Just get your files off the container - pronto :)

**Install docker**

To run a container on your local machine or laptop, download the docker program from:

[https://www.docker.com/](https://www.docker.com/)

There is a tab at the top of the page that says 'Get Docker'. You can get it for a windows or mac laptop or desktop. There are also options for AWS or Azure. If you want help using these cloud services, shoot me an email. (It's best to start running locally and understand that first).

**Run docker**

Once you have the docker program installed, open the program (you should get a terminal screen with command line).

Enter the command:
```
docker pull mccahill/jupyter-hts-2017
```

This will pull down the course docker image from dockerhub.  It may take a few minutes. Next, run the command to start a container:

```
docker run --name hts-course \
    -v YOUR_DIRECTORY_WITH_COURSE_MATERIALS:/home/jovyan/work \
    -d -p 127.0.0.1\:9999\:8888 \
    -e PASSWORD="YOUR_CHOSEN_NOTEBOOK_PASSWORD" \
    -e NB_UID=1000  -t mccahill/jupyter-hts-2017
```
The most important parts of this verbiage are the `YOUR_DIRECTORY_WITH_COURSE_MATERIALS` and `YOUR_CHOSEN_NOTEBOOK_PASSWORD`   . The directory name is the one you extracted your course materials into. So, if you put them in your home directory, it might look something like:

```
-v /home/janice/2017-HTS-materials:/home/jovyan/work
```

The password is whatever you want to use to password protect your notebook. Now, this command is running the notebook so that it is only 'seen' by your local computer - no one else on the internet can access it, and you cannot access it remotely, so the password is a bit of overkill. Use it anyway.

An example might be: 
```
-e PASSWORD="Pssst_this_is_Secret"   
```

except that this is a terrible password and you should follow standard rules of not using words, use a mix of capital and lowercase and special symbols. etc.  

The `-d -p 127.0.0.1\:9999\:8888` part of the command is telling docker to run the notebook so that it is only visible to the local machine. It is absolutely possible to run it as a server to be accessed across the web - but there are some security risks associated, so if you want to do this a) proceed with great caution and b) contact me if you need help.

**Open the notebook in your browser**

Open a browser and point it to 127.0.0.1:9999

You should get to a Jupyter screen asking for a password. This is the password you created in the docker run command.

Now, you should be able to run anything you like from the course. Depending on your laptop's resources (ram, cores), this might be slow, so be aware and start by testing only one file (vs the entire course data set).

**Using servers, etc.**

_Duke Affiliated peeps: _(From Mark McCahill) If you want to use the HTS Docker container for research,  contact Mark Delong and the research computing people - Andy Ingham (one of Mark DeLong’s guys) can help with setup of a research VM with the HTS Docker container. If there is enough interest we could also look at having an option for an HTS image that could be automatically provisioned as part of the Research Toolkits/RAPID service for Duke researchers in general.

_Non-Duke Affiliated peeps:_  Your institution's sys admin may be able to help set you up with compute resources if you let him/her know that you want to use a dockerhub image. I'm happy to consult with your IT people to get you up and running.

