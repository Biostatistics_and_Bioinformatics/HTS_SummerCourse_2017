# Review Questions

## Week 4

### Lectures

- Simulation (CC, KO)
- Reproducible analysis (CC, KO)
- Multiple testing
- Generalized Linear Models
- Big data and distributed computing

### Questions

**1**. Which of the following is true of the variable `results` after running the following R code to simulate coin tosses?

```
results <- sample(c("Heads" , "Tails"), size=10)
```

1. [ ] It is a vector of 10 strings
2. [ ] It is a vector of 20 strings
3. [ ] It is a vector of 2 strings
4. [ ] It is a 2 by 10 matrix
5. [ ] The statement fails to execute

**2**. Which of the following is true of the variable `results` after running the following R code?

```
ngenes <- 10000
nsubjects <- 100
results <- colMeans(replicate(ngenes, rnorm(nsubjects)))
```

1. [ ] It is a 10000 by 100 matrix
2. [ ] It is a 100 by 10000 matrix
3. [ ] It is a vector of length 10000
4. [ ] It is a vector of length 100
5. [ ] It is a scalar

**3**. Suppose the following code is executed. Which statement is true?

```R
N <- sample(1:10000, 1)
s <- 0
nexpts <- 100
nsubjects <- 10
ps <- rep(NA, nexpts)
for (i in 1:nexpts) {
  set.seed(N)
  x <- rnorm(nsubjects)
  y <- rnorm(nsubjects)
  ps[i] <- t.test(x, y)$p.value
}
s <- sum(ps < 0.05)
```

1. [ ] There is an approximately 5 percent chance that `s` is 0.
2. [ ] There is an approximately 5 percent chance that `s` is 100.
3. [ ] The most likely value of `s` is 5.
4. [ ] We need to adjust the values in `ps` for multiple testing.
5. [ ] We need to adjust the value of `s` for multiple testing.

**4**. Which of the following practices is **least** recommended for reproducible analysis?

1. [ ] Keep code under version control with frequent logged commits.
2. [ ] Intersperse code and documentation using literate programming.
3. [ ] Using copy and paste to minimize typing errors.
4. [ ] Making the raw data read-only and providing a checksum to avoid changes.
5. [ ] Writing tests to detect potential bugs in the code.

**5**. You have count data outcomes that you think can be appropriate fitted using a generalized linear model with 3 continuous predictor variables. Which is the appropriate family of Generalized Linear Model to use?

1. [ ] binomial(link = "logit")
2. [ ] gaussian(link = "identity")
3. [ ] Gamma(link = "inverse")
4. [ ] inverse.gaussian(link = "1/mu^2")
5. [ ] poisson(link = "log")

**6**. Suppose your code consists of 4 sections that can be run in parallel and 2 sections that must be run sequentially, and that the sequential portion of the code takes 10% of the total run-time when run using a single execution thread. What is the maximum speed-up if you can use an unlimited number of processors?

1. [ ] 1$\times$
2. [ ] 2$\times$
3. [ ] 4$\times$
3. [ ] 10$\times$
4. [ ] Infinity

**7**. You have a computational problem that consists of aligning billions of RNA-sequence reads of approbately 250 nucleotides each against a reference genome. Each read can be aligned independently, and the total size of the sequences is 10 TB (1 TB = 1,000 GB). Which is the most appropriate approach to significantly accelerate the alignment?

1. [ ] Vectorization to take advantage of pipelined computing
2. [ ] Multi-core parallelism using threads or processes
3. [ ] Massively parallel processing using Graphical Processing Units (GPU)
4. [ ] Distributed computing using a cluster
5. [ ] Peer-to-peer distributed computing over the internet

**8**. An experiment has a binary outcome `y`. You want to fit an additive regression model using three variables `x1`, `x2` and `x3`. Which of the following describes appropriate R code if you have a data frame with four columns `y`, `x1`, `x2` and `x3`?

1. [ ] `fit <- logistic(y ~ x1 + x2 + x3, data=df)`
2. [ ] `fit <- logit(y ~ x1 + x2 + x3, data=df)`
3. [ ] `fit <- glm(y ~ x1 + x2 + x3, family="binomial", data=df)`
4. [ ] `fit <- glm(y ~ x1 + x2 + x3, family="poisson", data=df)`
5. [ ] `fit <- glm(y ~ x1 + x2 + x3, family="normal", data=df)`

**9**. You run a simulation generating $n_1$ subjects in group 1 $\mathcal{N}(\mu_1, \sigma_1)$ and $n_2$ subjects in group 2 distributed as $\mathcal{N}(\mu_2, \sigma_2)$. You then estimate the power for a t-test to evaluate if there is a difference between means in the two groups by counting the fraction of experiments where the p-value is below some level $\alpha$. Which of the following changes is most likely to consistently increase the power calculated in your simulations?

1. [ ] Decrease the value of $\alpha$
2. [ ] Increase the absolute value of $\mu_1 - \mu_2$
3. [ ] Decrease the absolute value of $\mu_1 - \mu_2$
2. [ ] Increase the absolute value of $\sigma_1 - \sigma_2$
3. [ ] Decrease the absolute value of $\sigma_1 - \sigma_2$

**10**. You perform independent t-tests to compare the expression value of 10 genes in two groups of subjects. The smallest p-value found in the 10 tests was 0.001. You adjust the p-values using a Bonferroni correction to evaluate if any of the genes is differentially expressed between the two groups with a nominal Family-Wise Error Rate (FWER) of 0.05. If the genes are positively correlated with each other, which of the following statements is correct?

1. [ ] The smallest Bonferroni-adjusted p-value is 0.01 which is larger than the p-value from a FWER adjustment that took into account the genetic correlations
2. [ ] The smallest Bonferroni-adjusted p-value is 0.01 which is smaller than the p-value from a FWER adjustment that took into account the genetic correlations
3. [ ] The smallest Bonferroni-adjusted p-value is 0.01 which is the same as than the p-value from a FWER adjustment that took into account the genetic correlations
4. [ ] The smallest Bonferroni-adjusted p-value is 0.0001 which is larger than the p-value from a FWER adjustment that took into account the genetic correlations
5. [ ] The smallest Bonferroni-adjusted p-value is 0.0001 which is smaller than the p-value from a FWER adjustment that took into account the genetic correlations
