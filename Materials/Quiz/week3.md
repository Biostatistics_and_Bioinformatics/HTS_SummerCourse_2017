# Review Questions

## Week 3

### Lectures

- R graphics (CC, JM)
- Unsupervised (CC, JM)
- Supervised learning (CC, JM)

### Questions

**1**. Suppose you wanted to display a heatmap of the log ratio of gene counts in  treatment and control conditions to find genes which are under- or over-expressed. Which type of color palette is most appropriate?

1. [ ] Sequential
2. [ ] Divergent
3. [ ] Qualitative
4. [ ] ColorBrewer
5. [ ] Bright

**2**. Which of the following is **least** appropriate to display the spread of numerical values in several columns (e.g. if each column is normalized gene count and rows are subjects).

1. [ ] Swarm plots
2. [ ] Box plots
3. [ ] Bar chart
4. [ ] Overlay histograms
5. [ ] Violin plots

**3**. Which of the following `ggplot` concepts best describes the grammar of graphics principle of mapping data to visual attributes?

1. [ ] Geometries
2. [ ] Aesthetics
3. [ ] Palettes
4. [ ] Facets
5. [ ] Themes

**4**. Which of the following best describes the R formula `y ~ u + v + u:w - 1`?

1. [ ] Outcome `y` is related to `u`, `v` and the sum `u+v` after subtracting all values by one
2. [ ] Outcome `y` is related to `u`, `v` and the sum `u+v` with an L1 penalty
3. [ ] Outcome `y` is related to `u`, `v` and the product `uv` with y-intercept offset by -1
4. [ ] Outcome `y` is related  to `u`, `w` and the product `uv` and an unknown y-intercept5.
5. [ ] Outcome `y` is related to `u`, `v` and the product `uv` with y-intercept at 0

**5**. You run a RNA-sequencing experiment to try to predict the weight  of 10 rats. Using data from these 10 rats, you first find the top 10 genes with the highest absolute correlation to the outcome (time) and then use these 10 genes to fit an additive multivariate linear regression model. You find that the model predicts the weights of the 10 rats very well (very small residuals). Which critique is **least** justified?

1. [ ] Correction for multiple testing to adjust p-values should be performed.
2. [ ] There is "double-dipping" in the way the top 10 genes are used as predictor variables in the regression.
3. [ ] The model is likely to be over-fitted.
4. [ ] Making predictions on the training set will lead to over-optimistic evaluation of the model.
5. [ ] Cross-validation to evaluate model accuracy should be performed.

**6**. Suppose you perform a test of the hypothesis that two variables `x` and `y` are correlated (using `cor.test(x, y)` with Pearson correlation). The p-value will be equivalent to

1. [ ] `t.test(x, y)`
2. [ ] `wilcox.test(x, y)`
3. [ ] performing the correlation test with Spearman correlation
4. [ ] testing if the y-intercept of `lm(y ~ x)` is equal to 0
5. [ ] testing if the slope of `lm(y ~ x)` is equal to 0

**7**. What is the effect of the R command `hclust(dist(df), method = "complete")`?

1. [ ] Perform an agglomerative hierarchical clustering of `df`, at each step using imputation to complete missing data
2. [ ] Perform an agglomerative hierarchical clustering of `df`, at each step using multiple distance measures and return the best fitting model
3. [ ] Perform an agglomerative hierarchical clustering of `df`, at each step combing the two clusters with the shortest distance between the two centroids of each cluster
4. [ ] Perform an agglomerative hierarchical clustering of `df`, at each step combing the two clusters with the shortest distance between the two nearest members of each cluster
5. [ ] Perform an agglomerative hierarchical clustering of `df`, at each step combing the two clusters with the shortest distance between the two furthest members of each cluster

**8**. How many distances need to be computed to find the pairwise distance matrix between 10 genes?

1. [ ] 10
2. [ ] 100
3. [ ] 55
4. [ ] 45
5. [ ] 1024

**9**. Which statement is false given the following table?

|             | Test negative | Test positive |
|:------------|:--------------|:--------------|
| No disease  | 10            | 20            |
| Has disease | 30            | 40            |

1. [ ] There are 10 true negatives
2. [ ] There are 20 false positives
3. [ ] The test accuracy is 1/2.
4. [ ] The test sensitivity is 2/3.
5. [ ] The test specificity is 3/7.

**10**. Which R command will generate Figure 1 with an appropriate data frame `df`?

![`Figure 1`](figs/image01.svg)

1. [ ] `ggplot(df, aes(x=mpg, y=wt, color=as.factor(gear)) + geom_point(shape=1) + geom_smooth(method = lm)`
2. [ ] `ggplot(df, aes(x=mpg, y=wt, color=as.factor(gear)) + geom_point(shape=1) + geom_smooth(method = lm)") + facet_wrap(~ gear)`
3. [ ] `ggplot(df, aes(x=mpg, y=wt, color=as.factor(gear)) + geom_point(shape=1) + geom_smooth(method = lm) + facet_grid(wt ~ mpg)`
4. [ ] `ggplot(df, aes(x=mpg, y=wt, color=as.factor(gear)) + geom_point(shape=1) + geom_smooth(method = lm) + facet_grid(~ gear)`
5. [ ] `ggplot(df, aes(x=mpg, y=wt) + geom_point(shape=1, color=gear) + geom_smooth(method = lm, color=gear)`
