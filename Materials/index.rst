.. Duke HTS 2017 documentation master file, created by
   sphinx-quickstart on Fri Jun 30 14:44:44 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Duke HTS 2017
=========================================

Week 1
-------

General
^^^^^^^
- :download:`High-Throughput Sequencing Course Welcome <Docs/welcome_hts_2017.pdf>`
- :download:`2017 High-Throughput Sequencing Course: Final Schedule <Docs/HTS_Course_Schedule_Final_2017.pdf>`

Statistics
^^^^^^^^^^

- :download:`Introduction to Statistics <Statistics/07062017/stat-introduction.pdf>`
- :download:`Introduction to Statistics (Handout) <Statistics/07062017/stat-introduction-handout.pdf>`

- :download:`Statistical Inference: Part I <Statistics/07062017/stat-inference.pdf>`
- :download:`Statistical Inference: Part I (Handout) <Statistics/07062017/stat-inference-handout.pdf>`

Computation
^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   Computation/Wk1_Day2_AM/1_IntroToCompBoot.ipynb
   Computation/Wk1_Day2_PM/2_IntroductionToR.ipynb
   Computation/Wk1_Day3_PM/3_Custom_Functions_Mapping.ipynb
   .. Computation/Wk1_Day3_PM/4_Data_And_DataFrames.ipynb
   Computation/Wk1_Day3_PM/4_Data_And_DataFrames-Solutions.ipynb
   Computation//Wk1_Day4_AM/FridayMorningExercises.ipynb
   Computation/Wk1_Day4_AM/FridayMorningExercises-Solutions.ipynb

Biology
^^^^^^^^

- :download:`RNA-Seq Sample Preparation Theory <Biology/0707_rnaseq_prep.pdf>`

Week 2
-------

Computation
^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   Computation/Wk2_Day1_AM/WarmUpExercises.ipynb
   Computation/Wk2_Day1_AM/WarmUpExercises-Solutions.ipynb
   Computation/Wk2_Day1_AM/MondayMorningExercises.ipynb
   Computation/Wk2_Day1_AM/MondayMorningExercises-Solutions.ipynb

Biology
^^^^^^^

- :download:`Molecular Biology Lab Intro<Biology/protocols/Molecular_Biology_Lab_Intro.pdf>`
- :download:`Ribo-Zero Protocol <Biology/protocols/ribo-zero-reference-guide-15066012-02_trimmed.pdf>`
- :download:`Protocol for NEBNext Ultra Directional RNA Library Prep Kit<Biology/protocols/manualE7420_v8_trimmed.pdf>`
- :download:`magnetic_bead_MSDS.pdf <Biology/protocols/MSDSs/magnetic_bead_MSDS.pdf>`
- :download:`manualE6609.pdf <Biology/protocols/original_manuals/manualE6609.pdf>`
- :download:`manualE7420_v8.pdf <Biology/protocols/original_manuals/manualE7420_v8.pdf>`
- :download:`ribo-zero-reference-guide-15066012-02.pdf <Biology/protocols/original_manuals/ribo-zero-reference-guide-15066012-02.pdf>`
- :download:`RNACleanConcentrator5_ver2_2_1.pdf <Biology/protocols/original_manuals/RNACleanConcentrator5_ver2_2_1.pdf>`

Week 3
-------

Statistics
^^^^^^^^^^
- :download:`Experimental Design Part I <Statistics/07172017/ExpDesign.part_I.pdf>`
- :download:`Experimental Design Part II <Statistics/07172017/ExpDesign.part_II.pdf>`

- `RNA-seq: technical variability and sampling <https://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-12-293>`_
- `RNA-seq: An assessment of technical reproducibility and comparison with gene expression arrays <http://genome.cshlp.org/content/18/9/1509.long>`_

- :download:`Statistical Estimation <Statistics/07182017/stat-estimation.pdf>`
- :download:`Statistical Estimation (Handout) <Statistics/07182017/stat-estimation-handout.pdf>`

- :download:`Sources of Variability <Statistics/07182017/stat-source-of-variability.pdf>`
- :download:`Sources of Variability (Handout) <Statistics/07182017/stat-source-of-variability-handout.pdf>`

- :download:`Unsupervised Learning <Statistics/07192017/stat-unsupervised.pdf>`
- :download:`Unsupervised Learning (Handout) <Statistics/07192017/stat-unsupervised-handout.pdf>`

- :download:`Supervised Learning <Statistics/0720217/stat-supervised.pdf>`
- :download:`Supervised Learning (Handout) <Statistics/0720217/stat-supervised-handout.pdf>`

- :download:`RPC <Statistics/stat-ROC.pdf>`
- :download:`ROC (Handout) <Statistics/stat-ROC-handout.pdf>`

Computation
^^^^^^^^^^

.. toctree::
   :maxdepth: 1

   Computation//Wk3_Day2_PM/01-Stat-Inference.ipynb
   Computation//Wk3_Day2_PM/02-Stat-Estimation.ipynb
   Computation//Wk3_Day2_PM/03-Sources of Variability.ipynb
   Computation/Wk3_Day3_PM/Unsupervised_Learning.ipynb
   Computation//Wk3_Day4_PM/Supervised_Learning.ipynb

Week 4
-------

Statistics
^^^^^^^^^^

- :download:`Count Models <Statistics/07242017/stat-GLM-model-RNA-Seq.pdf>`
- :download:`Count Models (Handout) <Statistics/07242017/stat-GLM-model-RNA-Seq-handout.pdf>`
- :download:`Gene Networks <Statistics/07262017/GeneNetwork.pptx>`
- :download:`Gene Networks (R script) <Statistics/07262017/rna-transform-class.R>`
- :download:`Gene Networks (Data files) <Statistics/07262017/rnaseq_lusc_example_SeqQC.Rda>`

Bioinformatics
^^^^^^^^^^^^^^

- :download:`Bioinformatics Overview <Computation//Wk4_Day4_AM/bioinformatics_overview.pdf<>`


.. toctree::
  :maxdepth: 1

  Computation/Wk4_Day4_AM/filled_nb/2017_checkf.ipynb
  Computation/Wk4_Day4_AM/filled_nb/check_ds3000_for_unique_id.ipynb
  Computation/Wk4_Day4_AM/filled_nb/counting.ipynb
  Computation/Wk4_Day4_AM/filled_nb/demultiplex.ipynb
  Computation/Wk4_Day4_AM/filled_nb/fastq_intro.ipynb
  Computation/Wk4_Day4_AM/filled_nb/fastq_trimming.ipynb
  Computation/Wk4_Day4_AM/filled_nb/fastqc.ipynb
  Computation/Wk4_Day4_AM/filled_nb/full_2015_data_demux.ipynb
  Computation/Wk4_Day4_AM/filled_nb/full_2015_data_pipeline.ipynb
  Computation/Wk4_Day4_AM/filled_nb/full_pilot_data_pipeline.ipynb
  Computation/Wk4_Day4_AM/filled_nb/igv_visualization.ipynb
  Computation/Wk4_Day4_AM/filled_nb/loop_pipeline.ipynb
  Computation/Wk4_Day4_AM/filled_nb/mapping.ipynb
  Computation/Wk4_Day4_AM/filled_nb/paired_pilot_data_pipeline.ipynb
  Computation/Wk4_Day4_AM/filled_nb/paired_pilot_data_pipeline_unfiltered.ipynb
  Computation/Wk4_Day4_AM/filled_nb/quality_scores.ipynb
  Computation/Wk4_Day4_AM/prep_nb/make_clean_notebooks.ipynb

Computation
^^^^^^^^^^

.. toctree::
  :maxdepth: 1

  Computation/Wk4_Day1_AM/Review.ipynb
  Computation/Wk4_Day1_AM/Review_Solutions.ipynb
  Computation/Wk4_Day1_AM/Using_ggplot2.ipynb
  Computation/Wk4_Day1_AM/Using_ggplo2_Solutions.ipynb
  Computation//Wk4_Day2_PM/ROC.ipynb
  Computation/Wk4_Day2_PM/Count_Models.ipynb
  Computation/Wk4_Day2_PM/Count_Models_Solutions.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell_01___File_and_Directory_Management.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell_02___Working_with_Text.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell_03___Finding_Stuff.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell_04___Regular_Expresssions.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell_05___Shell_Scripts.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell___Exercises.ipynb
  Computation/Wk4_Day3_PM/The_Unix_Shell___Exercises_Solutions.ipynb
  Computation/Wk4_Day4_PM/Computing_Capstone.ipynb
  Computation/Wk4_Day4_PM/Computing_Capstone_Solutions.ipynb

Week 5
-------

Statistics
^^^^^^^^^^

- :download:`Time Course <Statistics/07312017/stat-TC.pdf>`
- :download:`Time Course (Handout) <Statistics/07312017/stat-TC-handout.pdf>`
- :download:`Censoring <Statistics/07312017/stat-censoring.pdf>`
- :download:`Censoring (Handout) <Statistics/07312017/stat-censoring-handout.pdf>`
- :download:`Setting up an RNA-Seq pipeline <Statistics/08032017/HTS-Week5.pptx>`

.. toctree::
  :maxdepth: 1

  Statistics/08032017/DESeq2-Notebook-introduction.ipynb
  Statistics/08032017/DESeq2-Notebook-2015-data.ipynb
  Statistics/08032017/STAR-pipeline.ipynb
  Statistics/08032017/GATK-reference.ipynb
  Statistics/08032017/GATK-pipeline-sample.ipynb

Bioinformatics
^^^^^^^^^^^^^^

- :download:`Data Standards <Bioinformatics/HTS_Course_2017_1_Data_Standards.pdf>`
- :download:`Data Sharing Provenance <Bioinformatics/HTS_Course_2017_2_Data_Sharing_Provenance.pdf>`
- :download:`Clinical Terminologies<Bioinformatics//HTS_Course_2017_3_Clinical_Terminologies.pdf>`
- :download:`Ontology <Bioinformatics/Ontology_lecture.pdf>`
- :download:`Short Read Alignment <Computation/Wk5_Day3_AM/ShortReadAlignment.pdf>`

Week 6
-------

Analysis of RNA-seq data
^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
  :maxdepth: 1

  Bioinformatics/2017_data_overview.ipynb
  Bioinformatics/HTS-Week6-DESeq2.ipynb
  Statistics/08032017/DESeq2-Notebook-2015-data.ipynb
