* Install Chrome
If you do not have the Chrome browser installed on your laptop, please do so now:
 [[https://www.google.com/chrome/browser/desktop/index.html][Install Chrome]]

* Claiming your Docker container for the course
  1. Log on to https://vm-manage.oit.duke.edu
  2. Click "Docker" on the upper right.
  3. Scroll to find the HTS_Course_2017 container and click on the link.
  4. You should now be connected to the Jupyter notebook, and you should see a file listing.
  
* In case of instructor updates to course materials, you may need to 'pull' from the course repository
    1. On the right hand side of the notebook screen, click 'New' and select 'Terminal'
    2. You will see a terminal screen and a unix prompt. Type 'cd HTS_Course_2017' and hit enter
    3. Type 'git pull' and hit enter
    4. Close the terminal screen
