#!/bin/bash

if [ -f $1 ]; then
    cat $1
else
    echo "No such file: $1"
fi